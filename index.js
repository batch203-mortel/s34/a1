// Load the express.js module into our application and saved it in a variable called express
// require ("package")
const express = require("express");

// localhost port number
const port = 4000;

// app is our server
// create an application that uses express and stores it as app
const app =express();

// middleware
// express.json() is a method which allow us to handle streaming of data and automically parse the incoming JSON from our req.body
app.use(express.json());

// mock data
let users = [
	{
		userName: "TStark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker"
	},
	{
		userName: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	}
];

let items = [
	{
		name: "Mjolnir",
		price: 50000,
		isActive: true
	},

	{
		name: "Vibranium Shield",
		price: 70000,
		isActive: true
	}
];

// Express has methods to use as routes corresponding to HTTP methods
// Syntax: app.method(<endpoint>, <function for req and res>)

app.get('/', (request, response) => {
	// res.send() actucally combines writeHead() and end()
	response.send(`Hello from my first expressJS API`);
	// response.status(200).send("message");
});

app.get('/greeting', (request, response) => {
	response.send(`Hello from Batch203-Mortel`);
});
// retrieval of the users in mock database
app.get('/users', (request, response) => {
	response.send(users);
});



app.get('/items', (request, response) => {
	response.send(items);
})

app.post('/items', (request, response) => {
	let newItem = {
		name: request.body.name,
		price: request.body.price,
		isActive: request.body.isActive
	};
	items.push(newItem);
	response.send(items);
});

app.put('/items/:index', (request, response) => {
	let index = parseInt(request.params.index);
	items[index].price = request.body.price;

	response.send(items[index]);
});
// port listener
app.listen(port, () => console.log(`Server is running at port ${port}.`));
